-- These aren't minetest aliases, they're mine
local aircraft_register_craft = aircraft_global.register_craft


aircraft_register_craft({
	output = "window",
	level = 0,
	recipe = "default:glass"
})
aircraft_register_craft({
	output = "window",
	level = 2,
	recipe = "default:obsidian_glass"
})


aircraft_register_craft({
	output = "engine",
	level = 0,
	recipe = "default:furnace",
})

aircraft_register_craft({
	output = "fast_engine",
	level = 0,
	recipe = "default:furnace",
})
aircraft_register_craft({
	output = "fast_engine",
	level = 2,
	required = {"terumet"},
	recipe = "terumet:mach_htfurn",
})


aircraft_register_craft({
	output = "sheet",
	level = 0,
	recipe = "default:paper",
})
aircraft_register_craft({
	output = "sheet",
	level = 1,
	required = {"basic_materials"},
	recipe = "basic_materials:plastic_sheet",
})


aircraft_register_craft({
	output = "rope",
	level = 0,
	recipe = "farming:cotton",
})
aircraft_register_craft({
	output = "rope",
	level = 1,
	recipe = "farming:string",
})
aircraft_register_craft({
	output = "rope",
	level = 2,
	required = {"ropes"},
	recipe = "ropes:ropesegment",
})

