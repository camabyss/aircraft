local aircraft_register_craft = aircraft_global.register_craft

aircraft_register_craft({
	output = "strong_metal_ingot",
	level = 1,
	recipe = {
		count = 3,
		{"default:steel_ingot",	"default:steel_ingot",		"default:steel_ingot"},
		{"default:coal_lump",	"default:coal_lump",		"default:coal_lump"	   },
		{"default:steel_ingot",	"default:steel_ingot",		"default:steel_ingot"},
	},
})

aircraft_register_craft({
	output = "strong_metal_block",
	level = 1,
})


aircraft_register_craft({
	output = "strong_metal_ingot",
	level = 1,
	required = {"terumet"},
	recipe = "terumet:ingot_tste"
})
aircraft_register_craft({
	output = "strong_metal_block",
	level = 1,
	required = {"terumet"},
	recipe = "terumet:block_tste"
})
aircraft_register_craft({
	output = "strong_metal_ingot",
	level = 1,
	required = {"basic_materials"},
	recipe = "basic_materials:brass_ingot"
})
aircraft_register_craft({
	output = "strong_metal_block",
	level = 1,
	required = {"basic_materials"},
	recipe = "basic_materials:brass_block"
})

aircraft_register_craft({
	output = "strong_metal_ingot",
	level = 0,
	recipe = "default:steel_ingot"
})
aircraft_register_craft({
	output = "strong_metal_block",
	level = 0,
	recipe = "default:steelblock"
})



aircraft_register_craft({
	output = "strong_metal_ingot",
	level = 3,
	required = {"titanium"},
	recipe = "titanium:titanium"
})
aircraft_register_craft({
	output = "strong_metal_block",
	level = 3,
	required = {"titanium"},
	recipe = "titanium:block"
})
