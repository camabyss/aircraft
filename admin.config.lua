
local physics = {
	-- Use a function if you want to change this dynamically
	gravity = -9.8,
	-- These things will also change per vehicle, these are just base factors
	air_resistance = 0.99,
	water_resistance = 0.9,
	ground_resistance = 0.8,
	burn_rate = 5,
	-- When something slows down it counts as "stopped" and physics will stop acting on it
	stop_speed = 0.1,
}


-- Speed will be naturally limited by air resistance
-- Use this switch to set a hard limit
-- Limits are set per vehicle, this is just a switch
local limit_speed = true

-- Height will be naturally limited by air thinning out
-- Use this switch to set a hard limit
-- This can be a number to set a hard ceiling
-- The default is 4000 if you just flag as true
local limit_height = true

-- Protection strategy
--====================--
        
local protection = true

--[[ VALUES:

-- false
-- Anyone can fly in any area

-- true
-- Protected areas will cause flying to trigger the default strategy (1)

-- <Number>
-- Can be used like a protection level
--      0 = no protection (false)
--      1 = default strategy (least cruel)
--      2+= more severe strategies
-- Use 1 to always choose the most gentle strategy
-- Use a high number like 100 to always choose the most severe strategy


-- <String>
-- Sets the strategy directly

-- "dampen" (1)
-- Slows down vehicles, forcing them to land in the area
-- Stops vehicles taking off

-- "block" (2)
-- Blocks vehicles from entering a protected area, like a wall

-- "airspace" (3)
-- Will use the "block" strategy, but will also block the space above the protection

-- "reverse" (4)
-- Will flip the flying direction if a protected area is hit

-- "kick" (5)
-- Will force players to dismount

-- !! See the protection.lua file for a current list of these values, this config may be out of date !!

]]






-- System stuff, just ignore
aircraft_global.protection = protection
aircraft_global.limit_height = limit_height
aircraft_global.limit_speed = limit_speed
aircraft_global.physics = physics

