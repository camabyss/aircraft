


-- Support for non-default games
if not default.player_attached then
	default.player_attached = {}
end

-- this should be the only global variable
aircraft_global = {}

aircraft_global.modpath = minetest.get_modpath("aircraft")
aircraft_global.armor_aircraft = {}



dofile(aircraft_global.modpath.."/config.lua")

-- Utilities
dofile(aircraft_global.modpath.."/physics.lua")
dofile(aircraft_global.modpath.."/functions.lua")
dofile(aircraft_global.modpath.."/entity_util.lua")
dofile(aircraft_global.modpath.."/interop/3d_armor.lua")

-- Vehicle definitions
dofile(aircraft_global.modpath.."/entities/plane_entity.lua")
dofile(aircraft_global.modpath.."/entities/biplane_entity.lua")
dofile(aircraft_global.modpath.."/entities/helicopter_entity.lua")
dofile(aircraft_global.modpath.."/entities/balloon_entity.lua")
dofile(aircraft_global.modpath.."/entities/glider_entity.lua")
dofile(aircraft_global.modpath.."/entities/parachute_entity.lua")

-- Additional stuff
dofile(aircraft_global.modpath.."/protection.lua")
dofile(aircraft_global.modpath.."/crafting.lua")
dofile(aircraft_global.modpath.."/armor.lua")


-- https://github.com/BlockySurvival/bls_mods.git
