

function glide_forward_if_falling(self, velo)
	if velo.y < -1 then
		self.v = self.v + (-1 * (velo.y + 1) * self.craft.lift * self.craft.wing_lift)
	end
end

function glide_lift(self, user_lift)
	local speed_lift = aircraft_global.physics.gravity
	-- speed makes lift if going fast enough
	if self.v > 1 then
		speed_lift = speed_lift + ((self.v - 1) * self.craft.lift)
	end
	-- level off if enough lift
	if speed_lift > 0 then
		speed_lift = 0
	end
	
	-- calculate extra lift due to flippers
	local wing_lift = 0
	if user_lift ~= 0 then
		wing_lift = self.v * self.craft.lift * user_lift * self.craft.wing_lift
	end

	return speed_lift + wing_lift
end
    