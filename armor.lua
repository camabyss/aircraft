
local function armor_types_player_index_craft(player, armor_inv, index, craft)
	local mod = aircraft_global.mod
	local stack = armor_inv:get_stack("armor", index)
	if stack:get_name() ~= mod(craft) then return end

	--local damage_amount = 100
	--local wear = stack:get_wear()
	--if (65535 - wear) <= damage_amount then return end

	local aircraft = minetest.add_entity(player:get_pos(), mod(craft))

	if not aircraft then return end

	-- Remove from armor inventory
	armor_inv:set_stack("armor", index, ItemStack({}))
	aircraft_global.armor:save_armor_inventory(player)
	aircraft_global.armor:set_player_armor(player)

	-- Position vehicle to match player
	aircraft:setyaw(player:get_look_horizontal())
	aircraft:setvelocity(player:get_player_velocity())

	aircraft_global.entity_util.mount_aircraft(aircraft:get_luaentity(), player)
	
	return true
end

local function armor_types_player_index(player, armor_inv, index)
	for _,craft in pairs(aircraft_global.armor_aircraft) do
		if armor_types_player_index_craft(player, armor_inv, index, craft) then
			return true
		end
	end
	return false
end

local function armor_types_player(player)

	local velocity = player:get_player_velocity()
	if velocity.y > -10 then return end

	local player_name = player:get_player_name()
	local armor_inv = minetest.get_inventory({
		type = "detached",
		name = player_name.."_armor"
	})

	for index = 1, 6 do
		if armor_types_player_index(player, armor_inv, index) then return end
	end
end

local check_period = 1
local timer = 0
local function armor_types(dtime)
	timer = timer + dtime
	if timer < check_period then return end
	timer = timer - check_period
	for _, player in ipairs(minetest.get_connected_players()) do
		armor_types_player(player)
	end
end

if table.getn(aircraft_global.armor_aircraft) then
	minetest.register_globalstep(armor_types)
end
