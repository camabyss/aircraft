
lever

light

button

dial

switch

screen

light metal

strong metal

engine
					strong metal
	piston			furnace			piston
	strong metal	rotor			strong metal

rotor
						strong metal
	steel	bearing		strong metal
						strong metal

piston
	strong metal	steel 	strong metal
	strong metal	steel 	strong metal
	strong metal	steel 	strong metal

rope
	string		string
	string		string		string
				string		string
	

metal rope
	light metal	light metal
	light metal	light metal	light metal
				light metal	light metal

prop hub
	strong metal strong metal strong metal
	strong metal bearing	  strong metal
	strong metal strong metal strong metal

bearing
	ballbearing ballbearing ballbearing
	ballbearing bearing hub ballbearing
	ballbearing ballbearing ballbearing

bearing hub
	light metal light metal light metal
				strong metal
	light metal light metal light metal

ball bearing
	strong metal

seat
	cotton	cotton
	cotton	wool	cotton
	cotton	cotton	cotton
	
small prop wing
	light metal
	light metal
	strong metal

prop wing
	strong metal	light metal
	strong metal	light metal
	strong metal	strong metal
		
large prop wing
	strong metal	strong metal	strong metal
	light metal		light metal		light metal
	strong metal	strong metal	strong metal
	


small propeller
					small prop wing
	small prop wing		prop hub	small prop wing
					small prop wing
		
propeller
				prop wing
	prop wing	prop hub	prop wing
				prop wing

large propeller
					large prop wing
	large prop wing		prop hub	large prop wing
					large prop wing

biplane
			propeller
	wing	cockpit		wing
			tail

	cockpit
										wood
		control panel	seat			wood
		engine  		wood			fuel tank

	fuel tank
		strong metal strong metal strong metal
		strong metal 			  strong metal
		strong metal strong metal strong metal

	control panel
		lever	dial	switch
		switch	steer w	dial
		lever	light	switch

	tail
						wood
		wood	wood	wood
						wood

	wing
		wood	wood		wood
				metal rope
		wood	wood		wood

helicopter
	large propeller	small propeller
	cockpit			tail
	legs
	
	legs
		leg		leg
	leg
		light metal			light metal
		wood		wood	wood
	
	tail
										light metal
		light metal		light metal		light metal
		light metal		light metal		light metal
	
	cockpit
		glass			glass			light metal
		control panel	seat			light metal
		engine  		light metal		fuel tank
						
	
	control panel
		lever	dial	switch
		switch	screen	dial
		button	light	lever

balloon
	sheet	light metal	sheet
	rope	heater		rope
			basket
	
	heater
				steel
		steel	furnace	steel
				steel
	
	basket
		rope	rope	rope
		wood	wood	wood
		rope	wood	rope
	
	sheet
		medium sheet	medium sheet
		medium sheet	medium sheet

	medium sheet
		small sheet		small sheet
		small sheet		small sheet

	small sheet
		cotton	cotton	cotton
		cotton	wool	cotton
		cotton	cotton	cotton
	
