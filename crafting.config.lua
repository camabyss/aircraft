
-- Crafting Level
--================
-- 0
-- Most basic crafting, keeps crafts to single levels where possible
-- Better for servers where flying is the main gimmick & everyone can do it
-- 1
-- Chooses more complex crafts, with better representation for functional parts regardless of how many sub-crafts there are
-- 2+
-- Increasingly complex and realistic crafts
-- If a crafting level cannot be met by any craft, the next biggest will be used

local crafting_level = 3


-- Custom Crafting
--=================
-- Set these to override the main crafts
-- this will stop any dynamism occurring and will not check for dependencies
-- these should be arrays for the key "recipe" when calling minetest.register_craft


local glider_craft = false
local parachute_craft = false

local biplane_craft = false
local balloon_craft = false

local plane_craft = false
local helicopter_craft = false

local strong_metal_ingot_craft = false
local light_metal_ingot_craft = false
local strong_metal_block_craft = false
local light_metal_block_craft = false

local biplane_tail_craft = false
local biplane_cockpit_craft = false
local biplane_wheels_craft = false
local biplane_wings_craft = false
local biplane_rotor_craft = false

local helicopter_tail_craft = false
local helicopter_tail_rotor_craft = false
local helicopter_cabin_craft = false
local helicopter_rotor_craft = false
local helicopter_legs_craft = false




-- Enable Dependencies
--=====================
-- You can disable dependencies if you have them but don't want to use them
-- Otherwise crafts will get more complex based on the optional dependencies you have

local basic_materials = true
local unified_inventory = true
local terumet = true
local ropes = true
local titanium = true










-- System stuff, just ignore

aircraft_global.crafting = {
	level = crafting_level,
	custom_crafts = {
		glider = glider_craft,
		parachute = parachute_craft,
		
		biplane = biplane_craft,
		balloon = balloon_craft,
		
		plane = plane_craft,
		helicopter = helicopter_craft,
		
		strong_metal_ingot = strong_metal_ingot_craft,
		light_metal_ingot = light_metal_ingot_craft,
		strong_metal_block = strong_metal_block_craft,
		light_metal_block = light_metal_block_craft,
		
		biplane_tail = biplane_tail_craft,
		biplane_cockpit = biplane_cockpit_craft,
		biplane_wheels = biplane_wheels_craft,
		biplane_wings = biplane_wings_craft,
		biplane_rotor = biplane_rotor_craft,

		helicopter_tail = helicopter_tail_craft,
		helicopter_tail_rotor = helicopter_tail_rotor_craft,
		helicopter_cabin = helicopter_cabin_craft,
		helicopter_rotor = helicopter_rotor_craft,
		helicopter_legs = helicopter_legs_craft,
	},
	mods_enabled = {
		basic_materials = basic_materials,
		unified_inventory = unified_inventory,
		terumet = terumet,
		ropes = ropes,
		titanium = titanium,
		
	}
}
