
--
-- Basic Crafting
--
local crafting = aircraft_global.crafting

local glider = crafting.custom_crafts.glider
local parachute = crafting.custom_crafts.parachute

local biplane = crafting.custom_crafts.biplane
local balloon = crafting.custom_crafts.glider

local plane = crafting.custom_crafts.plane
local helicopter = crafting.custom_crafts.helicopter

local strong_metal = crafting.custom_crafts.strong_metal
local light_metal = crafting.custom_crafts.light_metal

local helicopter_tail = crafting.custom_crafts.helicopter_tail
local helicopter_cabin = crafting.custom_crafts.helicopter_cabin
local helicopter_legs = crafting.custom_crafts.helicopter_legs
local helicopter_tail_rotor = crafting.custom_crafts.helicopter_tail_rotor
local helicopter_rotor = crafting.custom_crafts.helicopter_rotor

local biplane_rotor = crafting.custom_crafts.biplane_rotor
local biplane_wings = crafting.custom_crafts.biplane_wings
local biplane_wheels = crafting.custom_crafts.biplane_wheels
local biplane_tail = crafting.custom_crafts.biplane_tail
local biplane_cockpit = crafting.custom_crafts.biplane_cockpit

local crafts = {}

local function aircraft_register_craft(def)
	local craft_level = crafting.level
	local mods = crafting.mods_enabled
	if def.level > craft_level then return end
	if def.required then
		for k,req in pairs(def.required) do
			if (not mods[req])
			   or not minetest.global_exists(req)
			then return end
		end
	end
	local existing = {}
	if crafts[def.output] then
		existing = crafts[def.output]
	else
		crafts[def.output] = existing
	end
	
	table.insert(existing, def)
	
end

aircraft_global.register_craft = aircraft_register_craft

local function choose_craft(name)
	local craft_level = crafting.level
	local found = false
	if not crafts[name] then
		return found
	end
	for k,def in pairs(crafts[name]) do
		if (not found) or def.level > found.level then
			found = def
		end
	end
	if found == false then
		return false
	end
	return found.recipe
end



local custom_strong_metal = "aircraft:strong_metal"
local strong_metal_ = "aircraft_strong_metal"
local strong_metal_recipe = false

local custom_light_metal = "aircraft:light_metal"
local light_metal_ = "aircraft_light_metal"
local light_metal_recipe = false

if not strong_metal then
	if crafting.level == 0 then
		strong_metal = "default:steel"
	else
		strong_metal = custom_strong_metal
		dofile(aircraft_global.modpath.."/strong_metal.crafting.lua")
		strong_metal_recipe = choose_craft("strong_metal")
	end
end

if not light_metal then
	if crafting.level == 0 then
		light_metal = "default:tin"
	else
		light_metal = custom_light_metal
		dofile(aircraft_global.modpath.."/light_metal.crafting.lua")
		light_metal_recipe = choose_craft("light_metal")
	end
end

if type(strong_metal) == "table" then
	strong_metal_recipe = strong_metal
	strong_metal = custom_strong_metal
end
	

if type(light_metal) == "table" then
	light_metal_recipe = light_metal
	light_metal = custom_light_metal
end

aircraft_global.base_recipes = {
	strong_metal = strong_metal,
	light_metal = light_metal,
}


if (not plane) and aircraft_global.enabled.plane then

dofile(aircraft_global.modpath.."/entities/plane.crafting.lua")
plane = choose_craft("plane")

end

if (not biplane) and aircraft_global.enabled.biplane then

dofile(aircraft_global.modpath.."/entities/biplane.crafting.lua")
biplane = choose_craft("biplane")

if not biplane_rotor then
biplane_rotor = choose_craft("biplane_rotor")
end
if not biplane_tail then
biplane_tail = choose_craft("biplane_tail")
end
if not biplane_cockpit then
biplane_cockpit = choose_craft("biplane_cockpit")
end
if not biplane_wheels then
biplane_wheels = choose_craft("biplane_wheels")
end
if not biplane_wings then
biplane_wings = choose_craft("biplane_wings")
end

end

if (not helicopter) and aircraft_global.enabled.helicopter then

dofile(aircraft_global.modpath.."/entities/helicopter.crafting.lua")
helicopter = choose_craft("helicopter")
if not helicopter_cabin then
helicopter_cabin = choose_craft("helicopter_cabin")
end
if not helicopter_tail then
helicopter_tail = choose_craft("helicopter_tail")
end
if not helicopter_rotor then
helicopter_rotor = choose_craft("helicopter_rotor")
end
if not helicopter_legs then
helicopter_legs = choose_craft("helicopter_legs")
end
end

if (not balloon) and aircraft_global.enabled.balloon then
	
dofile(aircraft_global.modpath.."/entities/balloon.crafting.lua")
balloon = choose_craft("balloon")

end

if (not glider) and aircraft_global.enabled.glider then

dofile(aircraft_global.modpath.."/entities/glider.crafting.lua")
glider = choose_craft("glider")

end

if (not parachute) and aircraft_global.enabled.parachute then

dofile(aircraft_global.modpath.."/entities/parachute.crafting.lua")
parachute = choose_craft("parachute")

end



-- Actual minetest registration
if strong_metal_recipe then
minetest.register_craft({
	output = strong_metal.."_ingot",
	recipe = strong_metal_recipe
})
end

if strong_metal == custom_strong_metal then
		
	minetest.register_craftitem(strong_metal .. "_ingot", {
		description = "Strong Metal Ingot",
		inventory_image = strong_metal_ .. "_ingot.png",
	})
	
	
	minetest.register_node(strong_metal .. "block", {
		description = "Strong Metal Block",
		tiles = { strong_metal_ .. "_block.png" },
		is_ground_content = false,
		groups = {cracky=1, level=2},
		sounds = default.node_sound_metal_defaults()
	})

	minetest.register_craft( {
		type = "shapeless",
		output = strong_metal .. "_ingot 9",
		recipe = { strong_metal .. "block" },
	})

	minetest.register_craft( {
		output = strong_metal .. "block",
		recipe = {
			{ strong_metal .. "_ingot", strong_metal .. "_ingot", strong_metal .. "_ingot" },
			{ strong_metal .. "_ingot", strong_metal .. "_ingot", strong_metal .. "_ingot" },
			{ strong_metal .. "_ingot", strong_metal .. "_ingot", strong_metal .. "_ingot" },
		},
	})
end

if light_metal_recipe then
minetest.register_craft({
	output = light_metal.."_ingot",
	recipe = light_metal_recipe
})
end

if light_metal == custom_light_metal then
		
	minetest.register_craftitem(light_metal .. "_ingot", {
		description = "Light Metal Ingot",
		inventory_image = light_metal_ .. "_ingot.png",
	})
	
	minetest.register_node(light_metal .. "block", {
		description = "Light Metal Block",
		tiles = { light_metal_ .. "_block.png" },
		is_ground_content = false,
		groups = {cracky=1, level=2},
		sounds = default.node_sound_metal_defaults()
	})

	minetest.register_craft( {
		type = "shapeless",
		output = light_metal .. "_ingot 9",
		recipe = { light_metal .. "block" },
	})

	minetest.register_craft( {
		output = light_metal .. "block",
		recipe = {
			{ light_metal .. "_ingot", light_metal .. "_ingot", light_metal .. "_ingot" },
			{ light_metal .. "_ingot", light_metal .. "_ingot", light_metal .. "_ingot" },
			{ light_metal .. "_ingot", light_metal .. "_ingot", light_metal .. "_ingot" },
		},
	})
end




if aircraft_global.enabled.biplane and biplane then
minetest.register_craft({
	output = "aircraft:biplane",
	recipe = biplane
})
end

if aircraft_global.enabled.plane and plane then
minetest.register_craft({
	output = "aircraft:plane",
	recipe = plane
})
end

if aircraft_global.enabled.helicopter and helicopter then
minetest.register_craft({
	output = "aircraft:helicopter",
	recipe = helicopter
})
if helicopter_cabin then
		
	minetest.register_craftitem("aircraft:helicopter_cabin", {
		description = "Helicopter Cabin",
		inventory_image = "aircraft_item_helicopter_cabin.png",
	})
	minetest.register_craft({
		output = "aircraft:helicopter_cabin",
		recipe = helicopter_cabin
	})
end
if helicopter_tail then
		
	minetest.register_craftitem("aircraft:helicopter_tail", {
		description = "Helicopter Tail",
		inventory_image = "aircraft_item_helicopter_tail.png",
	})
	minetest.register_craft({
		output = "aircraft:helicopter_tail",
		recipe = helicopter_tail
	})
end
if helicopter_rotor then
		
	minetest.register_craftitem("aircraft:helicopter_rotor", {
		description = "Helicopter Rotor",
		inventory_image = "aircraft_item_helicopter_rotor.png",
	})
	minetest.register_craft({
		output = "aircraft:helicopter_rotor",
		recipe = helicopter_rotor
	})
end
if helicopter_legs then
		
	minetest.register_craftitem("aircraft:helicopter_legs", {
		description = "Helicopter Landing Gear",
		inventory_image = "aircraft_item_helicopter_legs.png",
	})
	minetest.register_craft({
		output = "aircraft:helicopter_legs",
		recipe = helicopter_legs
	})
end
end

if aircraft_global.enabled.balloon and balloon then
minetest.register_craft({
	output = "aircraft:balloon",
	recipe = balloon
})
end

if aircraft_global.enabled.glider and glider then
minetest.register_craft({
	output = "aircraft:glider",
	recipe = glider
})
end

if aircraft_global.enabled.parachute and parachute then
minetest.register_craft({
	output = "aircraft:parachute",
	recipe = parachute
})
end
