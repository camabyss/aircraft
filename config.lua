-- Basic configuration
--=====================
-- For more complex config options, and options to do with servers and OP control, see admin.config.lua
local mod_name = 'aircraft'

-- Basic Items
-- Easy to craft
-- Do not let you fly, only glide
local glider_enabled = true
local parachute_enabled = true

-- Utility Items
-- Reasonably simple to craft
-- Very low powered, so hard to fly
local biplane_enabled = true
local balloon_enabled = true

-- High-level Items
-- Complex crafts
-- Use fuel faster
-- Higher powered & easier to move in both directions (up & forward)
local plane_enabled = true
local helicopter_enabled = true





-- System stuff, just ignore
aircraft_global.mod_name = mod_name
local function mod(str, sep)
	if sep == nil then
		sep = ':'
	end
	return mod_name..sep..str
end
aircraft_global.mod = mod
aircraft_global.enabled = {
    glider = glider_enabled,
    parachute = parachute_enabled,
    
    biplane = biplane_enabled,
    balloon = balloon_enabled,
    
    plane = plane_enabled,
    helicopter = helicopter_enabled,
}

dofile(aircraft_global.modpath.."/crafting.config.lua")
dofile(aircraft_global.modpath.."/admin.config.lua")
