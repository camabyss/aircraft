local aircraft_register_craft = aircraft_global.register_craft
local mod = aircraft_global.mod


aircraft_register_craft({
	output = "plane",
	level = 0,
	recipe = {
		{mod("light_metal_ingot"),	mod("strong_metal_ingot"),	mod("light_metal_ingot")},
		{mod("light_metal_block"),	"default:glass",			mod("light_metal_block") },
		{"",	  		 			"group:wood",				""	   },
	},
})

aircraft_register_craft({
	output = "plane",
	level = 1,
	recipe = {
		{mod("strong_metal_ingot"),	"default:furnace",			mod("strong_metal_ingot") },
		{mod("light_metal_block"),	"default:obsidian_glass",	mod("light_metal_block") },
		{"",	  		 			mod("light_metal_block"),	""	   },
	},
})
