
--
-- Balloon entity
--

local balloon = {
	collisionbox = {-2, -0.45, -2, 2, 2, 2},
	mesh = "aircraft_balloon.obj",
	-- textures = {"aircraft_balloon.png"},

	heat = 0,
	craft = {
		turnable = false,
		fueled = true,
		name = 'balloon',
		label = 'Balloon',
		lift = 0.25,
		speed = 2,
		power = 0.1,
		wing_lift = 0.1,
		up_resistance = 0.9,
		forward_resistance = 0.9,
		user_position = {x = 0, y = -4, z = 0},
		staticdata = {"heat","y"},
		image = "balloon.png",
		states = {
			"aircraft_balloon.png",
			"aircraft_balloon_powered.png",
			"aircraft_balloon_open.png"
		}
	}
}


function balloon.on_step(self, dtime)
	local time = 1 + dtime
	
	local craft = self.craft
	local resistance = aircraft_global.physics.air_resistance
	local burn_rate = 3
	
	
	local velo = self.object:getvelocity()
	self.y = velo.y
	local position = self.object:getpos()
	local below = {x = position.x, z = position.z, y = position.y - 1}
	
	local infront = {x = position.x+(get_sign(velo.x)*2), z = position.z+(get_sign(velo.z)*2), y = position.y}
	local solid_ground = is_solid(below)

	
	local user = user_control(self.driver)
	if user.lift > 0 and self.heat < 50 then
		local used = use_fuel(self, craft.lift * burn_rate * time)
		if not used then
			user.lift = 0
			deactivate_aircraft(self)
		end
--	else
--		user.lift = 0
	end
	
	local state = 0
	if user.lift == 1 then
		state = state + 1
	end
	if user.lift == -1 then
		state = state + 2
	end
	
	set_state(self, state)

		-- accelerate
	if user.yaw ~= 0 and not solid_ground then
		velo.x = velo.x - (0.1 * user.yaw)
	end
	if user.power ~= 0 and not solid_ground then
		velo.z = velo.z + (0.1 * user.power)
	end
	
	local up_resistance = craft.up_resistance
	local forward_resistance = craft.forward_resistance
	
	if user.lift == 1 then
		self.heat = self.heat + (craft.power * time)
	end
	if user.lift == -1 then
		up_resistance = 1
		if self.heat > 1 then
			self.heat = self.heat - 1
		end
	end
	
	self.heat = self.heat * 0.999
	
	local heat_lift = self.heat * craft.lift
	
	local lift = heat_lift + aircraft_global.physics.gravity
	-- don't fall through the floor
	if is_solid(below) and lift < 0 then
		lift = 0
	end
	
	velo.y = velo.y + (lift * 0.04)
	
	-- return early if motionless ???
	if velo.x == 0 and velo.y == 0 and velo.z == 0 then
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = 0, z = 0})
		return
	end
	if is_water(below) or is_water(infront) then
		resistance = aircraft_global.physics.water_resistance
	end
	
	-- friction
	velo.x = velo.x * resistance * forward_resistance
	velo.z = velo.z * resistance * forward_resistance
	velo.y = velo.y * resistance * up_resistance
	
	-- stop if slow
	if math.abs(self.heat) < aircraft_global.physics.stop_speed then
		self.heat = 0
	end
	-- if math.abs(velo.y) < aircraft_global.physics.stop_speed then
	--	velo.y = 0
	-- end ]]
	
	-- return early if motionless
	if velo.x == 0 and velo.y == 0 and velo.z == 0 then
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = 0, z = 0})
		return
	end
	self.y = velo.y
	
	self.object:setpos(self.object:getpos())
 	self.object:setvelocity(velo)
end

register_aircraft(balloon)
