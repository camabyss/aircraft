local aircraft_register_craft = aircraft_global.register_craft
local mod = aircraft_global.mod

aircraft_register_craft({
	output = "helicopter",
	level = 0,
	recipe = {
		{mod("strong_metal_ingot"),	mod("strong_metal_block"),	mod("strong_metal_ingot")   },
		{mod("window"),				mod("light_metal_block"),	""  },
		{mod("light_metal_ingot"),	"group:wood",				mod("light_metal_ingot")   },
	},
})


aircraft_register_craft({
	output = "helicopter",
	level = 1,
	recipe = {
		{mod("strong_metal_ingot"),	mod("strong_metal_block"),	mod("strong_metal_ingot")   },
		{mod("window"),				mod("fast_engine"),			""  },
		{mod("light_metal_ingot"),	"group:wood",				mod("light_metal_ingot")   },
	},
})


aircraft_register_craft({
	output = "helicopter",
	level = 2,
	recipe = {
		{"",	mod("helicopter_rotor"),""},
		{"",	mod("helicopter_cabin"),mod("helicopter_tail")},
		{"",	mod("helicopter_legs"),	""},
	},
})
aircraft_register_craft({
	output = "helicopter_tail",
	level = 2,
	recipe = {
		{"",						"",							mod("strong_metal_block")},
		{"",						"",							mod("light_metal_ingot")},
		{mod("light_metal_ingot"),	mod("light_metal_ingot"),	mod("light_metal_ingot")},
	},
})

aircraft_register_craft({
	output = "helicopter_cabin",
	level = 2,
	recipe = {
		{"",						mod("light_metal_ingot"),mod("light_metal_ingot")},
		{mod("light_metal_ingot"),	mod("window"),			 mod("light_metal_ingot")},
		{mod("light_metal_ingot"),	mod("light_metal_ingot"),mod("fast_engine")},
	},
})
aircraft_register_craft({
	output = "helicopter_rotor",
	level = 2,
	recipe = {
		{"",						mod("strong_metal_ingot"),	""},
		{mod("strong_metal_ingot"),	mod("strong_metal_block"),	mod("strong_metal_ingot")},
		{"",						mod("strong_metal_ingot"),	""},
	},
})
aircraft_register_craft({
	output = "helicopter_legs",
	level = 2,
	recipe = {
		{"group:wood",				"",							"group:wood"},
		{mod("light_metal_ingot"),	mod("light_metal_ingot"),	mod("light_metal_ingot")},
	},
})


aircraft_register_craft({
	output = "helicopter_tail",
	level = 3,
	recipe = {
		{"",						"",							mod("helicopter_tail_rotor")},
		{"",						"",							mod("light_metal_ingot")},
		{mod("light_metal_block"),	mod("light_metal_block"),	mod("light_metal_ingot")},
	},
})
aircraft_register_craft({
	output = "helicopter_tail_rotor",
	level = 3,
	recipe = {
		{mod("light_metal_ingot"),	mod("strong_metal_ingot"),	mod("light_metal_ingot")},
		{mod("strong_metal_ingot"),	mod("strong_metal_ingot"),	mod("strong_metal_ingot")},
		{mod("light_metal_ingot"),	mod("strong_metal_ingot"),	mod("light_metal_ingot")},
	},
})
aircraft_register_craft({
	output = "helicopter_rotor",
	level = 2,
	required = {"basic_materials"},
	recipe = {
		{"",						mod("strong_metal_ingot"),	""},
		{mod("strong_metal_ingot"),	"basic_materials:motor",	mod("strong_metal_ingot")},
		{"",						mod("strong_metal_ingot"),	""},
	},
})
aircraft_register_craft({
	output = "helicopter_tail_rotor",
	level = 3,
	required = {"basic_materials"},
	recipe = {
		{mod("light_metal_ingot"),	mod("strong_metal_ingot"),	mod("light_metal_ingot")},
		{mod("strong_metal_ingot"),	"basic_materials:motor",	mod("strong_metal_ingot")},
		{mod("light_metal_ingot"),	mod("strong_metal_ingot"),	mod("light_metal_ingot")},
	},
})
