
--
-- Parachute entity
--

local parachute = {
	collisionbox = {-2, -0.45, -2, 2, 2, 2},
	mesh = "aircraft_parachute.obj",
	textures = {"aircraft_parachute.png"},

	craft = {
		armor = true,
		turnable = true,
		fueled = false,
		glides = true,
		name = 'parachute',
		label = 'Parachute',
		user_position = {x = 0, y = -5, z = 0},
		staticdata = {"v"},
		lift = 3,
		speed = 2,
		power = 1,
		turn_speed = 0.01,
		wing_lift = 0.03,
		up_resistance = 1,
		image = "boats_inventory.png",
		forward_resistance = 1,
	}
}

  
function parachute.on_step(self, dtime)
	local time = 1 + dtime
	-- respond to game changing velocity (due to collisions)
	self.v = get_v(self.object:getvelocity()) * get_sign(self.v)
	
	local resistance = 0.99
	local craft = self.craft
	
	local user = user_control(self.driver)
	
	local velo = self.object:getvelocity()
	local position = self.object:getpos()
	local below = {x = position.x, z = position.z, y = position.y - 1}
	local infront = {x = position.x+(get_sign(velo.x)*2), z = position.z+(get_sign(velo.z)*2), y = position.y}
	local solid_ground = is_solid(below)

	
		-- accelerate
	if self.v ~= 0 and user.yaw ~= 0 then
		local yaw = self.object:getyaw()
		self.object:setyaw(yaw + (time * craft.turn_speed * user.yaw))
	end
	if not solid_ground then
		user.power = 0
	end
	if user.power ~= 0 then
		self.v = (1+(time * craft.power)) * user.power
	end
	
	-- return early if motionless ???
	if self.v == 0 and velo.x == 0 and velo.y == 0 and velo.z == 0 then
		self.object:setpos(self.object:getpos())
		return
	end
	if solid_ground then
		resistance = 0.8
	elseif is_water(below) or is_water(infront) then
		resistance = 0.9
	end
	
	if user.yaw ~= 0 then
		resistance = resistance * 0.98
	end
	if user.lift ~= 0 then
		resistance = resistance * 0.975
	end
	
	
	-- friction
	self.v = self.v * resistance
	velo.y = velo.y * (resistance - (craft.lift*craft.wing_lift))
	
	-- stop if slow
	if math.abs(self.v) < aircraft_global.physics.stop_speed then
		self.v = 0
	end
	if math.abs(velo.y) < aircraft_global.physics.stop_speed then
		velo.y = 0
	end
	
	-- return early if motionless
	if self.v == 0 and velo.y == 0 then
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = 0, z = 0})
		return
	end

	glide_forward_if_falling(self, velo)
	
	-- limit velocity
	if self.v > craft.speed then
		self.v = craft.speed
	end
	
	local lift = glide_lift(self, user.lift)
	-- don't fall through the floor
	if is_solid(below) and lift < 0 then
		lift = 0
	end
	
	new_acce = {x = 0, y = lift, z = 0}
	
	new_velo = get_velocity(self.v, self.object:getyaw(), velo.y)
	-- update
	self.object:setpos(self.object:getpos())
	self.object:setacceleration(new_acce)
 	self.object:setvelocity(new_velo)
end



register_aircraft(parachute)
