local aircraft_register_craft = aircraft_global.register_craft
local mod = aircraft_global.mod

aircraft_register_craft({
	output = "glider",
	level = 0,
	recipe = {
		{"default:stick",	"group:wood",		"default:stick"   },
		{mod("sheet"),		"default:stick",	mod("sheet")  },
		{"",				"default:stick",	""	   },
	},
})

aircraft_register_craft({
	output = "glider",
	level = 1,
	recipe = {
		{"group:wood",	"group:wood",	"group:wood"   },
		{mod("sheet"),	"group:wood",	mod("sheet")  },
		{"",			"group:wood",	""	   },
	},
})
