--
-- Plane entity
--

local biplane = {
	collisionbox = {-2, -0.45, -2, 2, 2, 2},
	mesh = "aircraft_biplane.obj",
	textures = {"aircraft_biplane_unf.png"},

	craft = {
		turnable = true,
		fueled = true,
		activatable = true,
		glides = true,
		sitting = true,
		inactiveTextures = {"aircraft_biplane_unf.png"},
		activeTextures = {"aircraft_biplane_active.png"},
		name = 'biplane',
		label = 'BiPlane',
		lift = 4,
		speed = 6,
		power = 0.1,
		turn_speed = 0.005,
		wing_lift = 0.15,
		up_resistance = 0.95,
		forward_resistance = 1,
		user_position = {x = 0, y = 2, z = 0},
		image = "biplane.png",
		staticdata = {"v","y"}
	}
}

local function round(n) return math.floor(n+0.5) end
	
function biplane.on_step(self, dtime)
	--minetest.is_protected(pointed_thing.above, placer:get_player_name().."hi") 
	        
	local time = 1 + dtime
	-- respond to game changing velocity (due to collisions)
	local velo = self.object:getvelocity()
	self.v = get_v(velo) * get_sign(self.v)
	self.y = velo.y
	
	
	local ang = math.atan2(velo.z, velo.x)
	local x_u = math.cos(ang)
	local z_u = math.sin(ang)
	local forward = { x = round(x_u), z = round(z_u) }
	
	local resistance = aircraft_global.physics.air_resistance
	local craft = self.craft
	local up_resistance = craft.up_resistance
	local forward_resistance = craft.forward_resistance
	
	local position = self.object:getpos()
	local below = {x = position.x, z = position.z, y = position.y - 1}
	local infront = {
		x = position.x+(get_sign(velo.x)*2),
		z = position.z+(get_sign(velo.z)*2),
		y = position.y
	}
	local solid_ground = is_solid(below)
	
	if	self.driver and
		minetest.is_protected({
			x = position.x+forward.x,
			z = position.z+forward.z,
			y = position.y
		}, self.driver:get_player_name().."hi") then
		minetest.log("error", "protected")
	end
	
	local rate_alt = 1
	if self.active == false then
		rate_alt = 0.1
	end
	local user = user_control(self.driver)
	if user.power ~= 0 then
		local used = use_fuel(self, craft.power * aircraft_global.physics.burn_rate * time * rate_alt)
		if not used then
			user.power = 0
			deactivate_aircraft(self)
		end
	end

		-- accelerate
	if self.v ~= 0 and user.yaw ~= 0 then
		local yaw = self.object:getyaw()
		self.object:setyaw(yaw + (time * craft.turn_speed * user.yaw))
	end
	if self.active == false then
		if not solid_ground then
			user.power = 0
		end
		if user.power ~= 0 then
			self.v = (1+(time * craft.power)) * user.power
		end
	else
		if user.power >= 0 then
			self.v = self.v + (time * craft.power * user.power)
		end
	end
	
	-- return early if motionless ???
	if self.v == 0 and velo.x == 0 and velo.y == 0 and velo.z == 0 then
		self.object:setpos(self.object:getpos())
		return
	end
	if is_water(below) or is_water(infront) then
		resistance = aircraft_global.physics.water_resistance
	end

	if user.yaw ~= 0 then
		forward_resistance = forward_resistance * 0.98
	end
	if user.lift ~= 0 then
		forward_resistance = forward_resistance * 0.975
	end
	
	
	-- friction
	self.v = self.v * resistance * forward_resistance
	self.y = self.y * resistance * up_resistance
	
	-- stop if slow
	if math.abs(self.v) < aircraft_global.physics.stop_speed then
		self.v = 0
		if not solid_ground then
			deactivate_aircraft(self)
		end
	end
	if math.abs(self.y) < aircraft_global.physics.stop_speed then
		self.y = 0
	end
	
	-- return early if motionless
	if self.v == 0 and self.y == 0 then
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = 0, z = 0})
		return
	end

	-- TODO: self now has y, do same for glider
	glide_forward_if_falling(self, velo)
	
	-- limit velocity
	if self.v > craft.speed then
		self.v = craft.speed
	end
	
	local lift = glide_lift(self, user.lift)
	
	-- don't fall through the floor
	if is_solid(below) and lift < 0 then
		lift = 0
	end
	
	self.y = self.y + (lift * 0.04)
	local new_velo = get_velocity(self.v, self.object:getyaw(), self.y)
	-- update
	self.object:setpos(self.object:getpos())
	self.object:setvelocity(new_velo)
end



register_aircraft(biplane)
