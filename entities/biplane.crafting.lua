local aircraft_register_craft = aircraft_global.register_craft
local mod = aircraft_global.mod

aircraft_register_craft({
	output = "biplane",
	level = 0,
	recipe = {
		{"group:wood",			mod("strong_metal_ingot"),	"group:wood",},
		{"group:wood",			mod("window"),				"group:wood"},
		{"",	  		 		"group:wood",				""},
	},
})

aircraft_register_craft({
	output = "biplane",
	level = 1,
	recipe = {
		{mod("strong_metal_ingot"),	mod("engine"),	mod("strong_metal_ingot")},
		{"group:wood",				mod("window"),		"group:wood" },
		{"group:wood",				"group:wood",		"group:wood",},
	},
})


aircraft_register_craft({
	output = "biplane",
	level = 2,
	recipe = {
		{"",					mod("biplane_wings"),	""},
		{mod("biplane_rotor"),	mod("biplane_cockpit"),	mod("biplane_tail")},
		{"",					mod("biplane_wheels"),		"",},
	},
})

aircraft_register_craft({
	output = "biplane_rotor",
	level = 2,
	recipe = {
		{mod("strong_metal_ingot"),	"",							mod("strong_metal_ingot")},
		{"",						mod("strong_metal_block"),	""},
		{mod("strong_metal_ingot"),	"",							mod("strong_metal_ingot")},
	},
})
aircraft_register_craft({
	output = "biplane_rotor",
	level = 2,
	required = {"basic_materials"},
	recipe = {
		{mod("strong_metal_ingot"),	"",							mod("strong_metal_ingot")},
		{"",						"basic_materials:motor",	""},
		{mod("strong_metal_ingot"),	"",							mod("strong_metal_ingot")},
	},
})
aircraft_register_craft({
	output = "biplane_wings",
	level = 2,
	recipe = {
		{"group:wood",				"group:wood",	"group:wood"},
		{mod("light_metal_ingot"),	"",				mod("light_metal_ingot")},
		{"group:wood",				"group:wood",	"group:wood"},
	},
})
aircraft_register_craft({
	output = "biplane_cockpit",
	level = 2,
	recipe = {
		{"group:wood", mod("window"),"group:wood"},
		{mod("engine"),	"group:wood","group:wood"},
		{"group:wood",	"group:wood","group:wood"},
	},
})

aircraft_register_craft({
	output = "biplane_wheels",
	level = 2,
	recipe = {
		{""	,					mod("strong_metal_ingot"),	""},
		{"default:coal_lump",	mod("strong_metal_ingot"),	"default:coal_lump"},
		{"default:coal_lump",	"default:coal_lump",		"default:coal_lump"},
	},
})
aircraft_register_craft({
	output = "biplane_wheels",
	level = 4,
	required = {"terumet"},
	recipe = {
		{""	,					mod("strong_metal_ingot"),	""},
		{"terumet:item_rubber",	mod("strong_metal_ingot"),	"terumet:item_rubber"},
		{"terumet:item_rubber",	"terumet:item_rubber",		"terumet:item_rubber"},
	},
})
aircraft_register_craft({
	output = "biplane_wheels",
	level = 3,
	required = {"terumet"},
	recipe = {
		{""	,					mod("strong_metal_ingot"),	""},
		{"",					mod("strong_metal_ingot"),	""},
		{"",					"terumet:item_rubber",		""},
	},
})

aircraft_register_craft({
	output = "biplane_tail",
	level = 2,
	recipe = {
		{"","",							"group:wood"},
		{"group:wood",	"group:wood",	"group:wood"},
		{"group:wood",	"group:wood",	"group:wood"},
	},
})
