
--
-- Glider entity
--

local glider = {
	collisionbox = {-1.2, -0.45, -1.2, 1.2, 1.8, 1.2},
	mesh = "aircraft_glider.obj",
	textures = {"default_wood.png"}, --{"aircraft_glider.png"},

	craft = {
		turnable = true,
		fueled = false,
		glides = true,
		name = 'glider',
		label = 'Glider',
		lift = 4,
		speed = 4,
		power = 1,
		turn_speed = 0.01,
		wing_lift = 0.15,
		up_resistance = 1,
		forward_resistance = 1,
		user_position = {x = 0, y = -5, z = 0},
		image = "boats_inventory.png",
		staticdata = {"v"}
	}
}


function glider.on_step(self, dtime)
	        
	local time = 1 + dtime
	-- respond to game changing velocity (due to collisions)
	self.v = get_v(self.object:getvelocity()) * get_sign(self.v)
	
	local resistance = aircraft_global.physics.air_resistance
	local craft = self.craft
	
	local velo = self.object:getvelocity()
	local position = self.object:getpos()
	local below = {x = position.x, z = position.z, y = position.y - 1}
	local infront = {x = position.x+(get_sign(velo.x)*2), z = position.z+(get_sign(velo.z)*2), y = position.y}
	local solid_ground = is_solid(below)

	
	local up_resistance = craft.up_resistance
	local forward_resistance = craft.forward_resistance

	local user = user_control(self.driver)
	
		-- accelerate
	if self.v ~= 0 and user.yaw ~= 0 then
		local yaw = self.object:getyaw()
		self.object:setyaw(yaw + (time * craft.turn_speed * user.yaw))
	end
	if not solid_ground then
		user.power = 0
	end
	if user.power ~= 0 then
		self.v = (1+(time * craft.power)) * user.power
	end
	
	-- return early if motionless ???
	if self.v == 0 and velo.x == 0 and velo.y == 0 and velo.z == 0 then
		self.object:setpos(self.object:getpos())
		return
	end
	if solid_ground then
		resistance = aircraft_global.physics.ground_resistance
	elseif is_water(below) or is_water(infront) then
		resistance = aircraft_global.physics.water_resistance
	end
	if user.yaw ~= 0 then
		resistance = resistance * 0.98
	end
	if user.lift ~= 0 then
		resistance = resistance * 0.975
	end
	
	
	-- friction
	self.v = self.v * resistance * forward_resistance
	velo.y = velo.y * resistance * up_resistance
	
	-- stop if slow
	if math.abs(self.v) < aircraft_global.physics.stop_speed then
		self.v = 0
	end
	if math.abs(velo.y) < aircraft_global.physics.stop_speed then
		velo.y = 0
	end
	
	-- return early if motionless
	if self.v == 0 and velo.y == 0 then
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = 0, z = 0})
		return
	end

	glide_forward_if_falling(self, velo)
	-- limit velocity
	if self.v > craft.speed then
		self.v = craft.speed
	end
	local lift = glide_lift(self, user.lift)
	
	-- don't fall through the floor
	if is_solid(below) and lift < 0 then
		lift = 0
	end
	
	new_acce = {x = 0, y = lift, z = 0}
	
	new_velo = get_velocity(self.v, self.object:getyaw(), velo.y)
	-- update
	self.object:setpos(self.object:getpos())
	self.object:setacceleration(new_acce)
 	self.object:setvelocity(new_velo)
end


register_aircraft(glider)
