local aircraft_register_craft = aircraft_global.register_craft
local mod = aircraft_global.mod

aircraft_register_craft({
	output = "parachute",
	level = 0,
	recipe = {
		{mod("sheet"),	mod("sheet"),	mod("sheet")},
		{mod("rope"),	"",				mod("rope")},
		{"",	   		mod("rope"), 	""},
	},
})

aircraft_register_craft({
	output = "parachute",
	level = 1,
	required = {"unified_inventory"},
	recipe = {
		{mod("sheet"),	mod("sheet"),	mod("sheet")},
		{mod("rope"),	"",				mod("rope")},
		{"",	   		"unified_inventory:bag_medium",""},
	},
})
aircraft_register_craft({
	output = "parachute",
	level = 2,
	required = {"unified_inventory"},
	recipe = {
		{mod("sheet"),	mod("sheet"),	mod("sheet")},
		{mod("rope"),	"",				mod("rope")},
		{"",	   		"unified_inventory:bag_large",""},
	},
})
