local aircraft_register_craft = aircraft_global.register_craft
local mod = aircraft_global.mod

aircraft_register_craft({
	output = "balloon",
	level = 0,
	recipe = {
		{"default:paper",	"default:paper",	"default:paper"   },
		{"farming:cotton",	"group:wood",		"farming:cotton"  },
		{"",	   			"group:wood", 		""	   },
	},
})

aircraft_register_craft({
	output = "balloon",
	level = 1,
	recipe = {
		{"default:paper",	"default:paper",	"default:paper"   },
		{"farming:string",	"default:furnace",	"farming:string"  },
		{"",	   			"group:wood", 		""	   },
	},
})
