

--
-- Helicopter entity
--

local helicopter = {
	collisionbox = {-2, -0.45, -2, 2, 2, 2},
	mesh = "aircraft_helicopter.obj",
	textures = {"aircraft_helicopter.png"},

	heat = 0,
	yaw_speed = 0,
	craft = {
		must_be_inside = true,
		turnable = true,
		fueled = true,
		activatable = true,
		sitting = true,
		image = "helicopter.png",
		states = {
			"aircraft_helicopter.png",
			"aircraft_helicopter_active.png",
			"aircraft_helicopter_speed1.png",
			"aircraft_helicopter_speed2.png",
			"aircraft_helicopter_speed3.png",
			"aircraft_helicopter_speed4.png",
			"aircraft_helicopter_speed5.png",
			"aircraft_helicopter_speed6.png"
		},
		inactiveTextures = {"aircraft_helicopter.png"},
		activeTextures = {"aircraft_helicopter_active.png"},
		name = 'helicopter',
		label = 'Helicopter',
		lift = 3,
		speed = 3,
		power = 0.1,
		turn_speed = 0.005,
		wing_lift = 0.1,
		up_resistance = 0.95,
		forward_resistance = 0.95,
		user_position = {x = 0, y = 3.5, z = -2},
		staticdata = {"v","y","yaw_speed","heat"}
	}
}


function helicopter.on_step(self, dtime)
		        
	local time = 1 + dtime
	-- respond to game changing velocity (due to collisions)
	local velo = self.object:getvelocity()
	self.v = get_v(velo) * get_sign(self.v)
	self.y = velo.y
	
	local resistance = aircraft_global.physics.air_resistance
	local craft = self.craft
	
	local position = self.object:getpos()
	local below = {x = position.x, z = position.z, y = position.y - 1}
	local infront = {x = position.x+(get_sign(velo.x)*2), z = position.z+(get_sign(velo.z)*2), y = position.y}
	local solid_ground = is_solid(below)

	local user = user_control(self.driver)
	if user.lift ~= 0 and self.active then
		local used = use_fuel(self, craft.lift * aircraft_global.physics.burn_rate * time)
		if not used then
			user.lift = 0
			deactivate_aircraft(self)
		end
	else
		user.lift = 0
	end
	
	local up_resistance = craft.up_resistance
	local forward_resistance = craft.forward_resistance

	-- accelerate
	if user.yaw ~= 0 and not solid_ground then
		self.yaw_speed = self.yaw_speed + (craft.turn_speed * user.yaw * time)
	end
	if user.power ~= 0 and not solid_ground then
		self.v = self.v + (0.2 * user.power)
	end
	
	local up_resistance = craft.up_resistance
	local forward_resistance = craft.forward_resistance
	
	if user.lift == 1 then
		self.heat = self.heat + (craft.power * time)
	end
	
	self.heat = self.heat * 0.982
	local state = 0
	if self.active then
		state = math.ceil(self.heat)+1
	end
	set_state(self, state)
	
	local heat_lift = self.heat * craft.lift
	
	local lift = heat_lift + aircraft_global.physics.gravity
	-- don't fall through the floor
	if is_solid(below) and lift < 0 then
		lift = 0
	end
	
	self.y = self.y + (lift * 0.04)
	
	-- return early if motionless ???
	if self.v == 0 and velo.x == 0 and self.y == 0 and velo.z == 0 and self.yaw_speed == 0 and self.heat == 0 and solid_ground and lift == 0 then
		self.object:setyaw(self.object:getyaw())
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = self.y, z = 0})
		return
	end
	
	if is_water(below) or is_water(infront) then
		resistance = aircraft_global.physics.water_resistance
	end
	
	-- friction
	self.v = self.v * resistance * forward_resistance
	self.yaw_speed = self.yaw_speed * resistance * forward_resistance
	self.y = self.y * resistance * up_resistance
	
	-- stop if slow
	if math.abs(self.v) < aircraft_global.physics.stop_speed then
		self.v = 0
	end
	if math.abs(self.heat) < aircraft_global.physics.stop_speed then
		self.heat = 0
	end
	if math.abs(self.yaw_speed) < aircraft_global.physics.stop_speed/100 then
		self.yaw_speed = 0
	end
	if math.abs(self.y) < aircraft_global.physics.stop_speed then
		self.y = 0
	end
	
	-- return early if motionless
	if self.v == 0 and self.y == 0 and solid_ground and self.yaw_speed == 0 and lift == 0 then
		self.object:setyaw(self.object:getyaw())
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = self.y, z = 0})
		return
	end
	local new_yaw = self.object:getyaw() + self.yaw_speed
	self.object:setyaw(new_yaw)
	local new_velo = get_velocity(self.v, new_yaw, self.y)
	self.object:setpos(self.object:getpos())
 	self.object:setvelocity(new_velo)
end

register_aircraft(helicopter)

--[[

local helicopter_old = {
	collisionbox = {-2, -0.45, -2, 2, 2, 2},
	mesh = "aircraft_helicopter_old.obj",
	textures = {"aircraft_helicopter_old.png"},

	heat = 0,
	yaw_speed = 0,
	craft = {
		activatable = true,
		sitting = true,
		fueled = true,
		image = "helicopter.png",
		name = 'helicopter_old',
		label = 'Helicopter (Old)',
		inactiveTextures = {"aircraft_helicopter_old.png"},
		activeTextures = {"aircraft_helicopter_old_active.png"},
		lift = 3,
		speed = 3,
		power = 0.1,
		turn_speed = 0.005,
		wing_lift = 0.1,
		up_resistance = 0.95,
		forward_resistance = 0.95,
		user_position = {x = 0, y = 3.5, z = -2},
		staticdata = {"v","y","yaw_speed","heat"}
	}
}

register_aircraft(helicopter_old)



--
-- Helicopter entity
--
--[[
local helicopter_old = {
	collisionbox = {-2, -0.45, -2, 2, 2, 2},
	mesh = "aircraft_helicopter_old.obj",
	textures = {"aircraft_helicopter_old.png"},

	heat = 0,
	yaw_speed = 0,
	craft = {
		must_be_inside = true,
		turnable = true,
		fueled = true,
		activatable = true,
        sitting = true,
		image = "helicopter.png",
		name = 'helicopter_old',
		label = 'Helicopter (Old)',
		inactiveTextures = {"aircraft_helicopter_old.png"},
		activeTextures = {"aircraft_helicopter_old_active.png"},
		lift = 3,
		speed = 3,
		power = 0.1,
		turn_speed = 0.005,
		wing_lift = 0.1,
		up_resistance = 0.95,
		forward_resistance = 0.95,
		user_position = {x = 0, y = 3.5, z = -2},
		staticdata = {"v","yaw_speed","heat"}
	}
}


function helicopter_old.on_step(self, dtime)
		        
	local time = 1 + dtime
	-- respond to game changing velocity (due to collisions)
	self.v = get_v(self.object:getvelocity()) * get_sign(self.v)
	
	local resistance = aircraft_global.physics.air_resistance
	local craft = self.craft
	
	local velo = self.object:getvelocity()
	local position = self.object:getpos()
	local below = {x = position.x, z = position.z, y = position.y - 1}
	local infront = {x = position.x+(get_sign(velo.x)*2), z = position.z+(get_sign(velo.z)*2), y = position.y}
	local solid_ground = is_solid(below)

	local user = user_control(self.driver)
	if user.lift ~= 0 and self.active then
		local user = user_control(self.driver)
		if user.power ~= 0 then
			local used = use_fuel(self, craft.lift * aircraft_global.physics.burn_rate * time)
			if not used then
				user.lift = 0
				deactivate_aircraft(self)
			end
		end
	else
		user.lift = 0
	end
	
	local up_resistance = craft.up_resistance
	local forward_resistance = craft.forward_resistance

	-- accelerate
	if user.yaw ~= 0 and not solid_ground then
		self.yaw_speed = self.yaw_speed + (craft.turn_speed * user.yaw * time)
	end
	if user.power ~= 0 and not solid_ground then
		self.v = self.v + (0.2 * user.power)
	end
	
	local up_resistance = craft.up_resistance
	local forward_resistance = craft.forward_resistance
	
	if user.lift == 1 then
		self.heat = self.heat + (craft.power * time)
	end
	
	self.heat = self.heat * 0.982
	
	local heat_lift = self.heat * craft.lift
	
	local lift = heat_lift + aircraft_global.physics.gravity
	-- don't fall through the floor
	if is_solid(below) and lift < 0 then
		lift = 0
	end
	
	new_acce = {x = 0, y = lift, z = 0}
	
	-- return early if motionless ???
	if self.v == 0 and velo.x == 0 and velo.y == 0 and velo.z == 0 and self.yaw_speed == 0 and self.heat == 0 and solid_ground and lift == 0 then
		self.object:setyaw(self.object:getyaw())
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = 0, z = 0})
		self.object:setacceleration(new_acce)
		return
	end
	
	if is_water(below) or is_water(infront) then
		resistance = aircraft_global.physics.water_resistance
	end
	
	-- friction
	self.v = self.v * resistance * forward_resistance
	self.yaw_speed = self.yaw_speed * resistance * forward_resistance
	velo.y = velo.y * resistance * up_resistance
	
	-- stop if slow
	if math.abs(self.v) < aircraft_global.physics.stop_speed then
		self.v = 0
	end
	if math.abs(self.heat) < aircraft_global.physics.stop_speed then
		self.heat = 0
	end
	if math.abs(self.yaw_speed) < aircraft_global.physics.stop_speed/100 then
		self.yaw_speed = 0
	end
	if math.abs(velo.y) < aircraft_global.physics.stop_speed then
		velo.y = 0
	end
	
	-- return early if motionless
	if self.v == 0 and velo.y == 0 and solid_ground and self.yaw_speed == 0 and lift == 0 then
		self.object:setyaw(self.object:getyaw())
		self.object:setpos(self.object:getpos())
		self.object:setvelocity({x = 0, y = 0, z = 0})
		self.object:setacceleration(new_acce)
		return
	end
	local new_yaw = self.object:getyaw() + self.yaw_speed
	self.object:setyaw(new_yaw)
	local new_velo = get_velocity(self.v, new_yaw, velo.y)
	self.object:setpos(self.object:getpos())
 	self.object:setvelocity(new_velo)
	self.object:setacceleration(new_acce)
end

register_aircraft(helicopter_old)
]]
