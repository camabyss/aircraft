 
--
-- Helper functions
--

function is_water(pos)
	local nn = minetest.get_node(pos).name
	return minetest.get_item_group(nn, "water") ~= 0
end
function is_air(pos)
	local nn = minetest.get_node(pos).name
	return nn == 'air'
end
function is_solid(pos)
	return not (is_water(pos) or is_air(pos))
end

function get_sign(i)
	if i == 0 then
		return 0
	else
		return i / math.abs(i)
	end
end


function get_velocity(v, yaw, y)
	local x = -math.sin(yaw) * v
	local z =  math.cos(yaw) * v
	return {x = x, y = y, z = z}
end


function get_v(v)
	return math.sqrt(v.x ^ 2 + v.z ^ 2)
end

function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={}
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                table.insert(t, str)
        end
        return t
end
function tprint (tbl, indent)
  if not indent then indent = 0 end
  local ret = ""
  for k, v in pairs(tbl) do
	formatting = string.rep("  ", indent) .. k .. ": "
	if type(v) == "table" then
	  ret = ret .. formatting
	  ret = ret .. tprint(v, indent+1)
	elseif type(v) == 'boolean' then
	  ret = ret .. formatting .. tostring(v)   
	else
	  ret = ret .. formatting .. v
	end
  end
  return ret
end 

function is_creative(user)
	return creative and creative.is_enabled_for
					and creative.is_enabled_for(user:get_player_name())
end


function user_control(user)
	local ret = {lift=0,yaw=0,power=0}
	if user then
		local ctrl = user:get_player_control()
		if ctrl.jump and ctrl.sneak then
			ret.lift = -0.01
		elseif ctrl.jump then
			ret.lift = 1
		elseif ctrl.sneak then
			ret.lift = -1
		end
		if ctrl.up then
			ret.power = 1
		elseif ctrl.down then
			ret.power = -1
		end
		if ctrl.left and ctrl.right then
			ret.yaw = 0
		elseif ctrl.left then
			ret.yaw = 1
		elseif ctrl.right then
			ret.yaw = -1
		end
	end
	return ret
end

function use_fuel(aircraft, power_take)
	local good = true
	local fuel_remaining = aircraft.fuel_remaining or 0
	local fuel_totaltime = aircraft.fuel_totaltime or 0

	if fuel_totaltime ~= 0 and fuel_remaining >= power_take then
		-- The furnace is currently active and has enough fuel
		fuel_remaining = fuel_remaining - power_take
	else
		local inv = aircraft.inventory
		local fuellist = inv:get_list("fuel")
		local remainingfuel = fuellist
		local fuel
		local fuel_needed = power_take - fuel_remaining
		local update = true
		while update do
			update = false
			fuel, remaining_fuel_obj = minetest.get_craft_result({method = "fuel", width = 1, items = remainingfuel})
			if not is_creative(aircraft.driver) then
				remainingfuel = remaining_fuel_obj.items
			end
			local fuel_given = fuel.time
			if fuel_given == 0 then
				-- No valid fuel in fuel list
				fuel_remaining = 0
				fuel_totaltime = 0
			else
				if fuel_given < fuel_needed then
					fuel_needed = fuel_needed - fuel_given
					update = true
				else
					fuel_remaining = fuel_given - fuel_needed
					fuel_needed = 0
					fuel_totaltime = fuel_given
				end
			end
		end
		if fuel_totaltime == 0 then
			good = false
		end
		if good and not is_creative(aircraft.driver) then
			inv:set_list("fuel", remainingfuel)
		end
	end
	aircraft.fuel_remaining = fuel_remaining
	aircraft.fuel_totaltime = fuel_totaltime
	return good
end
