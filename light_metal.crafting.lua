local aircraft_register_craft = aircraft_global.register_craft


aircraft_register_craft({
	output = "light_metal_ingot",
	level = 0,
	recipe = "default:tin_ingot"
})

aircraft_register_craft({
	output = "light_metal_block",
	level = 0,
	recipe = "default:tinblock",
	
})

aircraft_register_craft({
	output = "light_metal_ingot",
	level = 1,
	recipe = {
		count = 3,
		{"default:tin_ingot",	"",		""},
		{"default:glass",		"",		""},
		{"default:tin_ingot",	"",		""},
	},
})

aircraft_register_craft({
	output = "light_metal_block",
	level = 1,
	recipe = {
		{"default:tin_ingot",	"default:tin_ingot",    "default:tin_ingot"},
		{"default:glass",		"default:glass",		"default:glass"	   },
		{"default:tin_ingot",	"default:tin_ingot",    "default:tin_ingot"},
	},
})
---[[
aircraft_register_craft({
	output = "light_metal_ingot",
	level = 1,
	required = {"terumet"},
	recipe = "terumet:ingot_ttin"
})

aircraft_register_craft({
	output = "light_metal_block",
	level = 1,
	required = {"terumet"},
	recipe = "terumet:block_ttin",
})
--]]
