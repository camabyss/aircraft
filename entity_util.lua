
local function is_placeable(pos)
	return is_solid(pos)
end

local function is_moveable(pos)
	return is_air(pos)
end


local function is_empty(aircraft)
	if not aircraft.inventory then
		return true
	end
	return minetest.get_craft_result({method="fuel", width=1, items={aircraft.inventory:get_stack("fuel", 1)}}).time == 0
end
local function is_usable(aircraft, user)
	if not user or not user:is_player() or aircraft.removed then
		return false
	end
	return true
end
local function is_driver(aircraft, user)
	return aircraft.driver and user == aircraft.driver
end
local function can_dig(aircraft, user)
	return is_usable(aircraft, user) and is_empty(aircraft)
end


-- TODO: re-implement these checks somehow?

local function allow_metadata_inventory_put(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	if listname == "fuel" then
		if minetest.get_craft_result({method="fuel", width=1, items={stack}}).time ~= 0 then
			return stack:get_count()
		else
			return 0
		end
	end
end

local function allow_metadata_inventory_move(pos, from_list, from_index, to_list, to_index, count, player)
	local meta = minetest.get_meta(pos)
	local inv = meta:get_inventory()
	local stack = inv:get_stack(from_list, from_index)
	return allow_metadata_inventory_put(pos, to_list, to_index, stack, player)
end

local function allow_metadata_inventory_take(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	return stack:get_count()
end


local aircraft_inventory_id = 0
local function init_aircraft_inventory(aircraft, existing)
	if not aircraft.inventory then
		local id = aircraft_inventory_id
		aircraft.inventory = minetest.create_detached_inventory_raw("aircraft_"..id)
		aircraft.inventory:set_size("fuel", 1)
		if existing ~= nil then
			aircraft.inventory:set_stack("fuel", 1, existing)
		end
		aircraft.inventoryid = aircraft_inventory_id
		aircraft_inventory_id = id + 1
	end
end

function set_state(aircraft, state)
	if aircraft.state == state then
		return
	end
	aircraft.state = state
	aircraft.object:set_properties({ textures = {aircraft.craft.states[state+1]} })
end

function activate_aircraft(aircraft)
	if aircraft.active or is_empty(aircraft) then
		return
	end
	aircraft.active = true
	aircraft.object:set_properties({ textures = aircraft.craft.activeTextures })
end
function deactivate_aircraft(aircraft)
	if not aircraft.active then
		return
	end
	aircraft.active = false
	aircraft.object:set_properties({ textures = aircraft.craft.inactiveTextures })
end

local function aircraft_formspec(aircraft)
	local fuel_percent = 0
	if aircraft.fuel_remaining ~= 0 and aircraft.fuel_totaltime ~= 0 then
		fuel_percent = math.floor(aircraft.fuel_remaining/aircraft.fuel_totaltime*100)
	end
	return "size[8,8.5]"..
	default.gui_bg..
	default.gui_bg_img..
	default.gui_slots..
	"list[detached:aircraft_"..aircraft.inventoryid..";fuel;3.5,1.75;1,1;]"..
	"image[3.5,0.5;1,1;default_furnace_fire_bg.png^[lowpart:"..
	(fuel_percent)..":default_furnace_fire_fg.png]"..
	"list[current_player;main;0,4.25;8,1;]"..
	"list[current_player;main;0,5.5;8,3;8]"..
	"listring[detached:aircraft_"..aircraft.inventoryid..";fuel]"..
	"listring[current_player;main]"..
	default.get_hotbar_bg(0, 4.25)
end



local function dismount_aircraft(aircraft)
	user = aircraft.driver
	aircraft.driver = nil
	user:set_detach()
	default.player_attached[user:get_player_name()] = false
	default.player_set_animation(user, "stand" , 30)
end
local function mount_aircraft(aircraft, user)
	local attach = user:get_attach()
    -- If user is attached to a different entity, detach them
	if attach and attach:get_luaentity() then
		local luaentity = attach:get_luaentity()
		if luaentity.driver then
			luaentity.driver = nil
		end
		user:set_detach()
	end
	aircraft.driver = user
	-- position determines outside view, not inside
	user:set_attach(aircraft.object, "",
		aircraft.craft.user_position, {x = 0, y = 0, z = 0})
	default.player_attached[user:get_player_name()] = true
end
local function remove_aircraft(aircraft, user)
	aircraft.removed = true
	local inv = user:get_inventory()
	if not is_creative(user)
			or not inv:contains_item("main", aircraft.craft_string) then
		local leftover = inv:add_item("main", aircraft.craft_string)
		-- if no room in inventory add a replacement plane to the world
		if not leftover:is_empty() then
			minetest.add_item(aircraft.object:getpos(), leftover)
		end
	end
	-- delay remove to ensure player is detached
	minetest.after(0.1, function()
		aircraft.object:remove()
	end)
    
end

local function on_punch(aircraft, puncher)
	
	if not is_usable(aircraft, puncher) then
		return
	end
	local ctrl
    
	if aircraft.craft.activatable then
		ctrl = puncher:get_player_control()
	end
	if (not aircraft.craft.activatable) or ctrl.sneak then
		if not can_dig(aircraft, puncher) then
			return
		end
		if is_driver(aircraft, puncher) then
			dismount_aircraft(aircraft)
		end
		if not aircraft.driver then
			remove_aircraft(aircraft, puncher)
		end
	elseif aircraft.craft.activatable then
		if aircraft.craft.must_be_inside and not (aircraft.driver and puncher == aircraft.driver) then
			return
		end
		if aircraft.active == true then
			deactivate_aircraft(aircraft)
			return
		end
		if aircraft.craft.fueled and is_empty(aircraft) then
			return
		end
		activate_aircraft(aircraft)
		return
	end
end

local function on_rightclick(aircraft, clicker)
	if aircraft.craft.fueled then
		init_aircraft_inventory(aircraft)
	end
	if not clicker or not clicker:is_player() then
		return
	end
	local ctrl
	if aircraft.craft.fueled then
		ctrl = clicker:get_player_control()
    end
	if (not aircraft.craft.fueled) or ctrl.sneak then
		if is_driver(aircraft, clicker) then
			dismount_aircraft(aircraft)
			local pos = clicker:getpos()
			pos = {x = pos.x, y = pos.y + 0.2, z = pos.z}
			minetest.after(0.1, function()
				clicker:setpos(pos)
			end)
		elseif not aircraft.driver then
			mount_aircraft(aircraft, clicker)
			if aircraft.craft.sitting then
				minetest.after(0.2, function()
					default.player_set_animation(clicker, "sit" , 30)
				end)
			end
			if aircraft.craft.turnable then
				clicker:set_look_horizontal(aircraft.object:getyaw())
			end
		end
    elseif aircraft.craft.fueled then
		minetest.show_formspec(clicker:get_player_name(), "Fueltank", aircraft_formspec(aircraft))
	end
end
    
local function get_staticdata(aircraft)
	local data = {}
	if aircraft.craft.fueled and aircraft.inventory then
		local inv = aircraft.inventory
		local fuellist = inv:get_stack("fuel", 1)
		local fuel_name = fuellist:get_name()
		local fuel_count = fuellist:get_count()
		local fuel_string = "0"

		-- TODO: use id to keep consistent when server is on??
		if fuel_name ~= "" and fuel_count ~= 0 then
			fuel_string = fuel_name.." "..fuel_count
		end
		data.fuel = fuel_string
		data.fuel_remaining = aircraft.fuel_remaining or 0
		data.fuel_totaltime = aircraft.fuel_totaltime or 0
	end
	for i, str in ipairs(aircraft.craft.staticdata) do
		data[str] = aircraft[str]
	end
	
	return minetest.write_json(data)
end

local function on_activate(aircraft, staticdata, dtime_s)
	local existing_inventory = false
	if staticdata and staticdata ~= "" and staticdata ~= nil then
		local obj = minetest.parse_json(staticdata)
		for name, val in pairs(obj) do
			if name == 'fuel' then
				if val ~= "0" then
					existing_inventory = val
				end
			else
				aircraft[name] = val
			end
		end
	end
	if aircraft.craft.fueled and existing_inventory then
		init_aircraft_inventory(aircraft, existing_inventory)
	end
	aircraft.object:set_armor_groups({immortal = 1})
end

function register_aircraft(conf)
	-- defaults for entities
	conf.visual = 'mesh'
	conf.physical = true

	-- defaults for drivables
	conf.removed = false
	conf.driver = nil

	-- generic interaction functions
	conf.on_punch = on_punch
	conf.on_rightclick = on_rightclick

	-- generic data functions
	conf.on_activate = on_activate
	conf.get_staticdata = get_staticdata
	
	conf.craft_string = "aircraft:"..conf.craft.name

	if conf.craft.fueled then
		conf.fuel_remaining = 0
		conf.fuel_totaltime = 0
	end
	if conf.craft.turnable then
		conf.v = 0
	end
	if conf.craft.activatable then
		conf.active = false
	end
	if conf.craft.states ~= nil then
		conf.state = 0
		conf.textures = {conf.craft.states[1]}
	end
		
	local function on_place(itemstack, placer, pointed_thing)
		local under = pointed_thing.under
		if minetest.is_protected(pointed_thing.above, placer:get_player_name().."hi") then
			return itemstack
		end
		local node = minetest.get_node(under)
		local udef = minetest.registered_nodes[node.name]
		if udef and udef.on_rightclick and
				not (placer and placer:get_player_control().sneak) then
			return udef.on_rightclick(under, node, placer, itemstack,
				pointed_thing) or itemstack
		end

		if pointed_thing.type ~= "node" then
			return itemstack
		end
		if not is_placeable(pointed_thing.under) then
			return itemstack
		end
		-- place above targeted block
		pointed_thing.under.y = pointed_thing.under.y + 1
		entity = minetest.add_entity(pointed_thing.under, conf.craft_string)
		if entity then
			if conf.craft.turnable then
				entity:setyaw(placer:get_look_horizontal())
			end
			if not is_creative(placer) then
				itemstack:take_item()
			end
		end
		return itemstack
	end
	minetest.register_entity(conf.craft_string, conf)
	
	if conf.craft.armor and minetest.global_exists("armor") and aircraft_global.armor then
		local mod = aircraft_global.mod
		table.insert(aircraft_global.armor_aircraft, conf.craft.name)
		aircraft_global.armor:register_armor(conf.craft_string, {
			description = conf.craft.label or conf.craft.name,
			inventory_image = conf.craft.image,
			texture = mod("armchest_"..conf.craft.name..".png","_"),
			preview = mod("prvchest_"..conf.craft.name..".png","_"),
			on_place = on_place,
			groups = {
				armor_use = 120,
				armor_heal = 0,
				armor_water = 0,
				armor_fire = 0,
				physics_speed = 0,
				physics_gravity = 0,
				physics_jump = 0,
				armor_torso = 1
			},
			armor_groups = {
				fleshy=1
			},
			damage_groups = {
				level = 1,
				choppy = 3,
				cracky = 3,
				snappy = 3,
				crumbly = 3
			}
		})
	else
		minetest.register_craftitem(conf.craft_string, {
			description = conf.craft.label or conf.craft.name,
			inventory_image = conf.craft.image,
			wield_image = conf.craft.image,
			wield_scale = {x = 2, y = 2, z = 1},
			stack_max = 1,
			on_place = on_place,
		})
	end
end

aircraft_global.entity_util = {
	mount_aircraft = mount_aircraft
}
