
--
-- Basic Crafting
--
local crafting = aircraft_global.crafting

local mod = aircraft_global.mod


local function metalBlock(metal,block)
	
	minetest.register_node(mod(metal.."_block"), {
		description = block,
		tiles = { mod(metal.."_block.png", "_") },
		is_ground_content = false,
		groups = {cracky=1, level=2},
		sounds = default.node_sound_metal_defaults()
	})

	minetest.register_craft( {
		type = "shapeless",
		output = mod(metal.."_ingot 9"),
		recipe = { mod(metal.."_block") },
	})

	minetest.register_craft( {
		output = mod(metal .. "_block"),
		recipe = {
			{ mod(metal.."_ingot"), mod(metal.."_ingot"), mod(metal.."_ingot") },
			{ mod(metal.."_ingot"), mod(metal.."_ingot"), mod(metal.."_ingot") },
			{ mod(metal.."_ingot"), mod(metal.."_ingot"), mod(metal.."_ingot") },
		},
	})
end

local function sort_by_tier(a,b)
	return a.tier > b.tier
end
local function doOnFail(craft)
	if not craft.doOnFail then return end
	if type(craft.doOnFail) == 'function' then
		craft.doOnFail()
	elseif type(craft.doAlso) == 'function' then
		craft.doAlso()
	end
end

local craft_config = {
	{
		name = 'glider',
		tier = 100,
		include = 'entities/glider.crafting.lua',
		required = true
	},
	{
		name = 'parachute',
		tier = 100,
		include = 'entities/parachute.crafting.lua',
		required = true
	},
	
	{
		name = 'biplane',
		tier = 100,
		include = 'entities/biplane.crafting.lua',
		required = true
	},
	{
		name = 'balloon',
		tier = 100,
		include = 'entities/balloon.crafting.lua',
		required = true
	},

	{
		name = 'plane',
		tier = 100,
		include = 'entities/plane.crafting.lua',
		required = true
	},
	{
		name = 'helicopter',
		tier = 100,
		include = 'entities/helicopter.crafting.lua',
		required = true
	},
	
	{
		name = 'engine',
		tier = 50,
	},
	{
		name = 'fast_engine',
		tier = 50,
	},
	{
		name = 'window',
		tier = 50,
	},
	{
		name = 'rotor_hub',
		tier = 50,
	},
	{
		name = 'rope',
		tier = 50,
	},
	{
		name = 'sheet',
		tier = 50,
	},
	
	{
		name = 'light_metal_ingot',
		tier = 10,
		include = 'light_metal.crafting.lua',
		craftItem = "Light Metal Ingot",
	},
	{
		name = 'strong_metal_ingot',
		tier = 10,
		include = 'strong_metal.crafting.lua',
		craftItem = "Strong Metal Ingot",
	},
	
	{
		name = 'light_metal_block',
		tier = 9,
		craftItem = "Light Metal Block",
		doOnFail = true,
		doAlso = function ()
			metalBlock("light_metal", "Light Metal Block")
		end
	},
	{
		name = 'strong_metal_block',
		tier = 9,
		include = 'strong_metal.crafting.lua',
		craftItem = "Strong Metal Ingot",
		doOnFail = true,
		doAlso = function ()
			metalBlock("strong_metal", "Strong Metal Block")
		end
	},
	
	{
		name = 'biplane_tail',
		tier = 99,
		craftItem = "Biplane Tail",
	},
	{
		name = 'biplane_rotor',
		tier = 99,
		craftItem = "Biplane Propeller",
	},
	{
		name = 'biplane_cockpit',
		tier = 99,
		craftItem = "Biplane Cockpit",
	},
	{
		name = 'biplane_wings',
		tier = 99,
		craftItem = "Biplane Wings",
	},
	{
		name = 'biplane_wheels',
		tier = 99,
		craftItem = "Biplane Landing Gear",
	},
	
	{
		name = 'helicopter_cabin',
		tier = 99,
		craftItem =  "Helicopter Cabin",
	},
	{
		name = 'helicopter_rotor',
		tier = 99,
		craftItem =  "Helicopter Rotor",
	},
	{
		name = 'helicopter_tail',
		tier = 99,
		craftItem =  "Helicopter Tail",
	},
	{
		name = 'helicopter_tail_rotor',
		tier = 98,
		craftItem =  "Helicopter Tail Rotor",
	},
	{
		name = 'helicopter_legs',
		tier = 99,
		craftItem =  "Helicopter Landing Gear",
	}
}


local crafts = {}

local function aircraft_register_craft(def)
	local craft_level = crafting.level
	local mods = crafting.mods_enabled
	if def.level > craft_level then return end
	if def.required then
		for k,req in pairs(def.required) do
			if (not mods[req])
				or not minetest.get_modpath(req)
			   --or not minetest.global_exists(req)
			then return end
		end
	end
	local existing = {}
	if crafts[def.output] then
		existing = crafts[def.output]
	else
		crafts[def.output] = existing
	end
	
	table.insert(existing, def)
	
end

aircraft_global.register_craft = aircraft_register_craft

local function choose_craft(name)
	local craft_level = crafting.level
	local found = false
	local found_level = 0
	if not crafts[name] then
		return found
	end
	for k,def in pairs(crafts[name]) do
		local new_level = (def.level*1000)
		if def.required then
			new_level = new_level + table.getn(def.required)
		end
		if (not found) or new_level > found_level then
			found = def
			found_level = new_level
		end
	end
	if found == false then
		return false
	end
	return found.recipe
end


dofile(aircraft_global.modpath.."/alias.crafting.lua")



local swapsies = {}
local function swap_string(item)
	local pointer = item
	while swapsies[pointer] do
		pointer = swapsies[pointer]
	end
	return pointer
end
local function swap(recipe)
	local ret = {}
	for i,row in ipairs(recipe) do
		if type(row) == 'string' then
			ret[i] = swap_string(row)
		elseif type(row) == 'table' then
			ret[i] = swap(row)
		end
	end
	return ret
end

local recipes = {}
local required_crafts = {}
local function craft_is_required(craft)
	for k,v in pairs(required_crafts) do
		if v == craft.name then
			return true
		end
	end
	return false
end
local function require_ingredients(recipe)
	local l = string.len(mod(""))
	for I,row in ipairs(recipe) do
		if type(row) == 'table' then
			require_ingredients(row)
		elseif type(row) == 'string' then
			if string.sub(row,1,l) == mod("") then
				table.insert(required_crafts, string.sub(row,l+1))
			end
		end
	end
end

local function make_craft(craft)
	if craft.include then
		dofile(aircraft_global.modpath.."/"..craft.include)
	end
	local custom_craft = crafting.custom_crafts[craft.name]

	-- if custom is string, ignore
	if type(custom_craft) == 'string' then
		swapsies[mod(craft.name)] = custom_craft
		return "named "..custom_craft
	end
	-- exit if not required
	if craft.required then
		-- cheaty: we're assuming all required crafts are also enable-able
		if not aircraft_global.enabled[craft.name] then
			return "not enabled"
		end
	else
		-- check if it's been used in a higher tier craft
		local required = craft_is_required(craft)
		if required == false then
			doOnFail(craft)
			return "not required"
		end
	end
	local recipe = false
	-- if table given, use that
	if type(custom_craft) == 'table' then
		recipe = custom_craft
	else
	-- otherwise, find a craft out of the possibilities
		recipe = choose_craft(craft.name)
	end
	-- recipe can be false if none is found
	if not recipe then
		doOnFail(craft)
		return "no recipe"
	end
	
	if type(recipe) == 'string' then
		swapsies[mod(craft.name)] = recipe
		return "swapped out"
	end
	
	-- at this point everything is valid.. continue safely
	recipes[craft.name] = recipe
	
	-- count what's required
	require_ingredients(recipe)
	
	-- craftItem should be the human description, or nil
	-- don't do this for vehicles, only parts
	if craft.craftItem then
		minetest.register_craftitem(mod(craft.name), {
			description = craft.craftItem,
			inventory_image = mod("item_"..craft.name..".png", "_"),
		})
	end
	
	-- there's always extras
	if craft.doAlso then
		craft.doAlso()
	end
	
	return "good"
end

-- sort by highest tier first
table.sort(craft_config, sort_by_tier)
for i,craft in ipairs(craft_config) do
	local output = make_craft(craft)
	minetest.log("error", craft.name.." "..tostring(output))
end

-- register the found recipes
for name,recipe in pairs(recipes) do
	-- append the count if given
	local output = mod(name)
	if recipe.count then
		output = output.." "..tostring(recipe.count)
	end
	-- do the thing
	minetest.register_craft({
		output = output,
		recipe = swap(recipe)
	})
end
