Crafting not complete, and fuel is vey limited, so use creative mode for testing.

All icons look like boats for now, sorry.

How To Fly
=========

Glider
------
* Right click to board
* Run off of a high place to start gliding
* Left/right to turn
* Space to gain lift (swoop up)
* Shift to shed lift (swoop down)
* Punch to remove

Parachute
------
* Right click to board
* Run off of a high place to start gliding
* Left/right to turn
* Space to gain lift (swoop up)
* Shift to shed lift (swoop down)
* Punch to remove

Plane
-----
* Right click to add fuel
* Punch to start
* Shift right click to board
* Forward to accelerate (will be in driving mode if inactive)
* Space to gain altitude
* Shift to lose altitude
* Left/right to turn
* Shift punch to remove

Helicopter
----------
* Right click to add fuel
* Shift right click to board
* Punch to start (must be boarded)
* Space to speed up wings (will lift you when fast enough)
* Forward / backward to move
* Left/right to turn
* Shift punch to remove

Balloon
-------
* Right click to add fuel
* Shift right click to board
* Space to heat up ballon (will lift you when hot enough, takes a long time)
* Forward / backward to move absolutely (no turning)
* Left / right to move absolutely (no turning)
* Shift to release air (drop and decrease heat)
* Punch to remove

